#setup key
mkdir ~/.oci
echo "[DEFAULT]" >> ~/.oci/config
echo "user=ocid1.user.oc1..aaaaaaaarej6kwjai5r5sawh6rw6pkdyiaxmxpk2lyui3b73x57dty5gszya" >> ~/.oci/config
echo "fingerprint=40:62:f2:c0:61:49:9c:73:72:41:4a:4f:52:3e:d2:89" >> ~/.oci/config
echo "region=us-ashburn-1" >> ~/.oci/config
echo "tenancy=ocid1.tenancy.oc1..aaaaaaaahm5ihdibbxp5qei5grbu7ctbw6pqci7j4isuep63t5hnxtdmrbba" >> ~/.oci/config
echo "key_file=~/.oci/key.pem" >> ~/.oci/config
echo "-----BEGIN RSA PRIVATE KEY-----
MIIEpQIBAAKCAQEAuSzZLLXyTUECeWjkV8EH81kdnPsMJaxcpTQhxrMEhFC1Q65G
wTOXJeglIKh2LEpT1qwG8iZLWCwgYLMOtaJrNjaNCTezNJvbP3CywaQpSN8ytwp+
1gPqigSZADM+fUUGZrJW+6ZbsiOwwJ8sIHM7PKyM+heq/UR3iq/WYZ17gjj4/SbS
ayNXRFImbsopkmVnVJeOAFqILDMRhWYnKYaygcqms+0ZfFk3K15xjBZ6bP3A5Heb
8Etu+iqIp14b7DvTiYYnqLA8Q5TJn5lypFdqAxwCT6jWjccuPoKjra9Ye9cpR2Uy
euuUE5els/blRycM9cS90lP0Xu4Jrm6srNVqHQIDAQABAoIBAAIYteTBspqZT9Pc
EinEL7jbm3A5j2ulxe7A5lj/kd+Fome/w2FQmjd9p131SlE6DjTyAaogMN4wsbUm
kdYQnrIgb/xYuxivYw4W0o9KsoT2sfZMkh8OMEubvbe43M5CR85F2qJCwesEaG3l
33QGmfcX5Qi31+HCMrBJSV4VkF5rDcGv8/YWv8Wzu5HFdgJyvuRvuwKOShhjdbaL
mvDfULjP/mZb7kSXP6ITa2mPef6O5pCJEr1BxAPyDEwmAnlCGq85JOVy5rlrjrZf
OQN2j6MPCz3Td2HyG7gXUPAEw5mafDt8vOfzg6R/kC2zEaS8qsnFexBBFB92BKL3
vShBc4ECgYEA8YndRLKt4ScwiTJhUViOzxDZ4pTdmxA76p5+SzqMSLWqncPC9lEg
MjdNeAJuMgQVL6jJBlH6coLuIJ4/ADQJjuNoTRa2Sieto0SpFhWZcoq5l1bBVA+F
b2k4p834H40C+v8usfVDgbdC7VfNnJAn1EmPagWW+Q/6O/LToWTfSHECgYEAxEMV
7D1jkY8oE2bV8cnLF4MCLFsrR0TUnZW3vAcKbINs5lCngYXId7se863sDyAll6NP
nGehR8dB18CQDtyRgvu9NXL+t1iHMLrgA063YHxKKc9CLSH4BzebDVqkuSaxC8pL
ETCjYbWeUczlMrNwjLRlV1didUHLDvH8/Bo1sm0CgYEAlO6nKubp5Dzf5pjT/O6Y
EAGZ9+ZGxog6Ca70vLo7KdIqg+DjZDIdohkLgYYuIFT4I0z1txnxTY6B+kbZHez4
P8zk0TPB7vOMU3HQuLdUyJXVXgW2a3iulEXTE1UjO4EdZ1rVlhFfMKqpb+OF+pXA
mVtKPNt5fdVmPHS8HXYvX2ECgYEAkDoAhVADuLcsKz3aIUo63jdfRHtnDYOuf3PD
xwChj2bxgztoLU3EKQCw2UBJhkAIgCv6WCTrp5TtdQDEnLdIk6otvHaaauA60Puz
nZqeNeCAm8L8jbA11g/t+N1QyeN+Hfz2N1a36hkOlGxvTlgBcNUs2AeWijbkY0I/
uIAGT+0CgYEAlgQ3hMKrlDjkRxoAWFGFEEeYWfwXCy1v2IesxPyxyqzDyR2/KzF3
2rUQlaeXvTAEX/wuAePK68mCspGZ3DnxCtHo8xRSGMCVp8TjOKHewbAhCI71TYcL
wQ8CLuryox78GqLUQTHvesSxANM0clxRrUH2k2SnnNjjh6J1TXEsL5A=
-----END RSA PRIVATE KEY-----" >> ~/.oci/key.pem
    
#install oci cli
curl -L -O https://raw.githubusercontent.com/oracle/oci-cli/master/scripts/install/install.sh
chmod +x install.sh
./install.sh --accept-all-defaults
echo "::add-path::/home/runner/bin"
exec -l $SHELL

#repair permission
oci setup repair-file-permissions --file /home/runner/.oci/config
oci setup repair-file-permissions --file /home/runner/.oci/key.pem

#setup kubeconfig
mkdir -p ~/.kube
oci ce cluster create-kubeconfig --cluster-id ocid1.cluster.oc1.iad.aaaaaaaaaeywezlegiztmzbvhe2wgnzvgu3gintemqygeojugcydiolfha2d --file $HOME/.kube/config --region us-ashburn-1 --token-version 2.0.0 
export KUBECONFIG=~/.kube/config

#update cluster
kubectl rollout restart deployment/spring-hello-development